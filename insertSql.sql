CREATE TABLE IF NOT EXISTS `mremoteng`.`tblcons` (
  `ID` MEDIUMINT(19) NOT NULL AUTO_INCREMENT,
  `ConstantID` VARCHAR(128) NULL DEFAULT NULL,
  `PositionID` INT(11) NOT NULL,
  `ParentID` VARCHAR(128) NULL DEFAULT NULL,
  `LastChange` VARCHAR(20) NOT NULL,
  `Name` VARCHAR(128) NOT NULL,
  `Type` VARCHAR(32) NOT NULL,
  `Expanded` BIT(1) NOT NULL,
  `Description` VARCHAR(1024) NULL DEFAULT NULL,
  `Icon` VARCHAR(128) NOT NULL,
  `Panel` VARCHAR(128) NOT NULL,
  `Username` VARCHAR(512) NULL DEFAULT NULL,
  `DomainName` VARCHAR(512) NULL DEFAULT NULL,
  `Password` VARCHAR(1024) NULL DEFAULT NULL,
  `Hostname` VARCHAR(512) NULL DEFAULT NULL,
  `Protocol` VARCHAR(32) NOT NULL,
  `PuttySession` VARCHAR(128) NULL DEFAULT NULL,
  `Port` INT(11) NOT NULL,
  `ConnectToConsole` BIT(1) NOT NULL,
  `RenderingEngine` VARCHAR(10) NULL DEFAULT NULL,
  `ICAEncryptionStrength` VARCHAR(32) NOT NULL,
  `RDPAuthenticationLevel` VARCHAR(32) NOT NULL,
  `Colors` VARCHAR(32) NOT NULL,
  `Resolution` VARCHAR(32) NOT NULL,
  `DisplayWallpaper` BIT(1) NOT NULL,
  `DisplayThemes` BIT(1) NOT NULL,
  `EnableFontSmoothing` BIT(1) NOT NULL,
  `EnableDesktopComposition` BIT(1) NOT NULL,
  `CacheBitmaps` BIT(1) NOT NULL,
  `RedirectDiskDrives` BIT(1) NOT NULL,
  `RedirectPorts` BIT(1) NOT NULL,
  `RedirectPrinters` BIT(1) NOT NULL,
  `RedirectSmartCards` BIT(1) NOT NULL,
  `RedirectSound` VARCHAR(64) NOT NULL,
  `RedirectKeys` BIT(1) NOT NULL,
  `Connected` BIT(1) NOT NULL,
  `PreExtApp` VARCHAR(255) NULL DEFAULT NULL,
  `PostExtApp` VARCHAR(255) NULL DEFAULT NULL,
  `MacAddress` VARCHAR(32) NULL DEFAULT NULL,
  `UserField` VARCHAR(255) NULL DEFAULT NULL,
  `ExtApp` VARCHAR(255) NULL DEFAULT NULL,
  `VNCCompression` VARCHAR(10) NULL DEFAULT NULL,
  `VNCEncoding` VARCHAR(20) NULL DEFAULT NULL,
  `VNCAuthMode` VARCHAR(10) NULL DEFAULT NULL,
  `VNCProxyType` VARCHAR(20) NULL DEFAULT NULL,
  `VNCProxyIP` VARCHAR(128) NULL DEFAULT NULL,
  `VNCProxyPort` INT(11) NULL DEFAULT NULL,
  `VNCProxyUsername` VARCHAR(512) NULL DEFAULT NULL,
  `VNCProxyPassword` VARCHAR(1024) NULL DEFAULT NULL,
  `VNCColors` VARCHAR(10) NULL DEFAULT NULL,
  `VNCSmartSizeMode` VARCHAR(20) NULL DEFAULT NULL,
  `VNCViewOnly` BIT(1) NOT NULL,
  `RDGatewayUsageMethod` VARCHAR(32) NOT NULL,
  `RDGatewayHostname` VARCHAR(512) NULL DEFAULT NULL,
  `RDGatewayUseConnectionCredentials` VARCHAR(32) NOT NULL,
  `RDGatewayUsername` VARCHAR(512) NULL DEFAULT NULL,
  `RDGatewayPassword` VARCHAR(1024) NULL DEFAULT NULL,
  `RDGatewayDomain` VARCHAR(512) NULL DEFAULT NULL,
  `InheritCacheBitmaps` BIT(1) NOT NULL,
  `InheritColors` BIT(1) NOT NULL,
  `InheritDescription` BIT(1) NOT NULL,
  `InheritDisplayThemes` BIT(1) NOT NULL,
  `InheritDisplayWallpaper` BIT(1) NOT NULL,
  `InheritEnableFontSmoothing` BIT(1) NOT NULL,
  `InheritEnableDesktopComposition` BIT(1) NOT NULL,
  `InheritDomain` BIT(1) NOT NULL,
  `InheritIcon` BIT(1) NOT NULL,
  `InheritPanel` BIT(1) NOT NULL,
  `InheritPassword` BIT(1) NOT NULL,
  `InheritPort` BIT(1) NOT NULL,
  `InheritProtocol` BIT(1) NOT NULL,
  `InheritPuttySession` BIT(1) NOT NULL,
  `InheritRedirectDiskDrives` BIT(1) NOT NULL,
  `InheritRedirectKeys` BIT(1) NOT NULL,
  `InheritRedirectPorts` BIT(1) NOT NULL,
  `InheritRedirectPrinters` BIT(1) NOT NULL,
  `InheritRedirectSmartCards` BIT(1) NOT NULL,
  `InheritRedirectSound` BIT(1) NOT NULL,
  `InheritResolution` BIT(1) NOT NULL,
  `InheritUseConsoleSession` BIT(1) NOT NULL,
  `InheritRenderingEngine` BIT(1) NOT NULL,
  `InheritICAEncryptionStrength` BIT(1) NOT NULL,
  `InheritRDPAuthenticationLevel` BIT(1) NOT NULL,
  `InheritUsername` BIT(1) NOT NULL,
  `InheritPreExtApp` BIT(1) NOT NULL,
  `InheritPostExtApp` BIT(1) NOT NULL,
  `InheritMacAddress` BIT(1) NOT NULL,
  `InheritUserField` BIT(1) NOT NULL,
  `InheritExtApp` BIT(1) NOT NULL,
  `InheritVNCCompression` BIT(1) NOT NULL,
  `InheritVNCEncoding` BIT(1) NOT NULL,
  `InheritVNCAuthMode` BIT(1) NOT NULL,
  `InheritVNCProxyType` BIT(1) NOT NULL,
  `InheritVNCProxyIP` BIT(1) NOT NULL,
  `InheritVNCProxyPort` BIT(1) NOT NULL,
  `InheritVNCProxyUsername` BIT(1) NOT NULL,
  `InheritVNCProxyPassword` BIT(1) NOT NULL,
  `InheritVNCColors` BIT(1) NOT NULL,
  `InheritVNCSmartSizeMode` BIT(1) NOT NULL,
  `InheritVNCViewOnly` BIT(1) NOT NULL,
  `InheritRDGatewayUsageMethod` BIT(1) NOT NULL,
  `InheritRDGatewayHostname` BIT(1) NOT NULL,
  `InheritRDGatewayUseConnectionCredentials` BIT(1) NOT NULL,
  `InheritRDGatewayUsername` BIT(1) NOT NULL,
  `InheritRDGatewayPassword` BIT(1) NOT NULL,
  `InheritRDGatewayDomain` BIT(1) NOT NULL,
  `UseCredSsp` BIT(1) NOT NULL DEFAULT b'1',
  `LoadBalanceInfo` VARCHAR(1024) NULL DEFAULT NULL,
  `InheritUseCredSsp` BIT(1) NOT NULL DEFAULT b'0',
  `AutomaticResize` BIT(1) NOT NULL DEFAULT b'1',
  `InheritLoadBalanceInfo` BIT(1) NOT NULL DEFAULT b'0',
  `InheritAutomaticResize` BIT(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 134
DEFAULT CHARACTER SET = utf8b





CREATE TABLE IF NOT EXISTS `mremoteng`.`tblroot` (
  `Name` VARCHAR(2048) NOT NULL,
  `Export` BIT(1) NOT NULL,
  `Protected` VARCHAR(4048) NOT NULL,
  `ConfVersion` FLOAT NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8


INSERT INTO `mremoteng`.`tblroot`
(`Name`,
`Export`,
`Protected`,
`ConfVersion`)
VALUES
'Connexions'
'0',
'q2kU8nO/BJTw8VjI6i1ErdOLBNi6J1rvz2dULvPOBROUZshXPQNSWydBTv1U+xNf',
'2.5');



CREATE TABLE IF NOT EXISTS `mremoteng`.`tblupdate` (
  `LastUpdate` DATETIME NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8

