Imports System.IO
Imports mRemoteNG.Messages
Imports mRemoteNG.My
Imports mRemoteNG.Connection.Protocol
Imports mRemoteNG.Root
Imports WeifenLuo.WinFormsUI.Docking
Imports System.Net.NetworkInformation
Imports mRemoteNG.App.Runtime
Imports MySql.Data.MySqlClient
Imports mRemoteNG.Tools.Misc


Namespace UI
    Namespace Window
        Public Class Config
            Inherits UI.Window.Base


#Region "Form Init"
            Friend WithEvents btnShowProperties As System.Windows.Forms.ToolStripButton
            Friend WithEvents btnShowDefaultProperties As System.Windows.Forms.ToolStripButton
            Friend WithEvents btnShowInheritance As System.Windows.Forms.ToolStripButton
            Friend WithEvents btnShowDefaultInheritance As System.Windows.Forms.ToolStripButton
            Friend WithEvents btnIcon As System.Windows.Forms.ToolStripButton
            Friend WithEvents btnHostStatus As System.Windows.Forms.ToolStripButton
            Friend WithEvents cMenIcons As System.Windows.Forms.ContextMenuStrip
            Private components As System.ComponentModel.IContainer
            Friend WithEvents propertyGridContextMenu As System.Windows.Forms.ContextMenuStrip
            Friend WithEvents propertyGridContextMenuShowHelpText As System.Windows.Forms.ToolStripMenuItem
            Friend WithEvents propertyGridContextMenuReset As System.Windows.Forms.ToolStripMenuItem
            Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
            Friend WithEvents pGrid As Azuria.Common.Controls.FilteredPropertyGrid
            Private Sub InitializeComponent()
                Me.components = New System.ComponentModel.Container()
                Me.pGrid = New Azuria.Common.Controls.FilteredPropertyGrid()
                Me.propertyGridContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
                Me.propertyGridContextMenuReset = New System.Windows.Forms.ToolStripMenuItem()
                Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
                Me.propertyGridContextMenuShowHelpText = New System.Windows.Forms.ToolStripMenuItem()
                Me.btnShowInheritance = New System.Windows.Forms.ToolStripButton()
                Me.btnShowDefaultInheritance = New System.Windows.Forms.ToolStripButton()
                Me.btnShowProperties = New System.Windows.Forms.ToolStripButton()
                Me.btnShowDefaultProperties = New System.Windows.Forms.ToolStripButton()
                Me.btnIcon = New System.Windows.Forms.ToolStripButton()
                Me.btnHostStatus = New System.Windows.Forms.ToolStripButton()
                Me.cMenIcons = New System.Windows.Forms.ContextMenuStrip(Me.components)
                Me.propertyGridContextMenu.SuspendLayout()
                Me.SuspendLayout()
                '
                'pGrid
                '
                Me.pGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
                Me.pGrid.BrowsableProperties = Nothing
                Me.pGrid.ContextMenuStrip = Me.propertyGridContextMenu
                Me.pGrid.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                Me.pGrid.HiddenAttributes = Nothing
                Me.pGrid.HiddenProperties = Nothing
                Me.pGrid.Location = New System.Drawing.Point(0, 0)
                Me.pGrid.Name = "pGrid"
                Me.pGrid.PropertySort = System.Windows.Forms.PropertySort.Categorized
                Me.pGrid.Size = New System.Drawing.Size(226, 530)
                Me.pGrid.TabIndex = 0
                Me.pGrid.UseCompatibleTextRendering = True
                '
                'propertyGridContextMenu
                '
                Me.propertyGridContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.propertyGridContextMenuReset, Me.ToolStripSeparator1, Me.propertyGridContextMenuShowHelpText})
                Me.propertyGridContextMenu.Name = "propertyGridContextMenu"
                Me.propertyGridContextMenu.Size = New System.Drawing.Size(157, 76)
                '
                'propertyGridContextMenuReset
                '
                Me.propertyGridContextMenuReset.Name = "propertyGridContextMenuReset"
                Me.propertyGridContextMenuReset.Size = New System.Drawing.Size(156, 22)
                Me.propertyGridContextMenuReset.Text = "&Reset"
                '
                'ToolStripSeparator1
                '
                Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
                Me.ToolStripSeparator1.Size = New System.Drawing.Size(153, 6)
                '
                'propertyGridContextMenuShowHelpText
                '
                Me.propertyGridContextMenuShowHelpText.Name = "propertyGridContextMenuShowHelpText"
                Me.propertyGridContextMenuShowHelpText.Size = New System.Drawing.Size(156, 22)
                Me.propertyGridContextMenuShowHelpText.Text = "&Show Help Text"
                '
                'btnShowInheritance
                '
                Me.btnShowInheritance.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                Me.btnShowInheritance.Image = Global.mRemoteNG.My.Resources.Resources.Inheritance
                Me.btnShowInheritance.ImageTransparentColor = System.Drawing.Color.Magenta
                Me.btnShowInheritance.Name = "btnShowInheritance"
                Me.btnShowInheritance.Size = New System.Drawing.Size(23, 22)
                Me.btnShowInheritance.Text = "Inheritance"
                '
                'btnShowDefaultInheritance
                '
                Me.btnShowDefaultInheritance.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                Me.btnShowDefaultInheritance.Image = Global.mRemoteNG.My.Resources.Resources.Inheritance_Default
                Me.btnShowDefaultInheritance.ImageTransparentColor = System.Drawing.Color.Magenta
                Me.btnShowDefaultInheritance.Name = "btnShowDefaultInheritance"
                Me.btnShowDefaultInheritance.Size = New System.Drawing.Size(23, 22)
                Me.btnShowDefaultInheritance.Text = "Default Inheritance"
                '
                'btnShowProperties
                '
                Me.btnShowProperties.Checked = True
                Me.btnShowProperties.CheckState = System.Windows.Forms.CheckState.Checked
                Me.btnShowProperties.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                Me.btnShowProperties.Image = Global.mRemoteNG.My.Resources.Resources.Properties
                Me.btnShowProperties.ImageTransparentColor = System.Drawing.Color.Magenta
                Me.btnShowProperties.Name = "btnShowProperties"
                Me.btnShowProperties.Size = New System.Drawing.Size(23, 22)
                Me.btnShowProperties.Text = "Properties"
                '
                'btnShowDefaultProperties
                '
                Me.btnShowDefaultProperties.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                Me.btnShowDefaultProperties.Image = Global.mRemoteNG.My.Resources.Resources.Properties_Default
                Me.btnShowDefaultProperties.ImageTransparentColor = System.Drawing.Color.Magenta
                Me.btnShowDefaultProperties.Name = "btnShowDefaultProperties"
                Me.btnShowDefaultProperties.Size = New System.Drawing.Size(23, 22)
                Me.btnShowDefaultProperties.Text = "Default Properties"
                '
                'btnIcon
                '
                Me.btnIcon.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
                Me.btnIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                Me.btnIcon.ImageTransparentColor = System.Drawing.Color.Magenta
                Me.btnIcon.Name = "btnIcon"
                Me.btnIcon.Size = New System.Drawing.Size(23, 22)
                Me.btnIcon.Text = "Icon"
                '
                'btnHostStatus
                '
                Me.btnHostStatus.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
                Me.btnHostStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                Me.btnHostStatus.Image = Global.mRemoteNG.My.Resources.Resources.HostStatus_Check
                Me.btnHostStatus.ImageTransparentColor = System.Drawing.Color.Magenta
                Me.btnHostStatus.Name = "btnHostStatus"
                Me.btnHostStatus.Size = New System.Drawing.Size(23, 22)
                Me.btnHostStatus.Tag = "checking"
                Me.btnHostStatus.Text = "Status"
                '
                'cMenIcons
                '
                Me.cMenIcons.Name = "cMenIcons"
                Me.cMenIcons.Size = New System.Drawing.Size(61, 4)
                '
                'Config
                '
                Me.ClientSize = New System.Drawing.Size(226, 530)
                Me.Controls.Add(Me.pGrid)
                Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                Me.HideOnClose = True
                Me.Icon = Global.mRemoteNG.My.Resources.Resources.Config_Icon
                Me.Name = "Config"
                Me.TabText = "Config"
                Me.Text = "Config"
                Me.propertyGridContextMenu.ResumeLayout(False)
                Me.ResumeLayout(False)

            End Sub
#End Region

#Region "Private Properties"
            Private ConfigLoading As Boolean = False
            Private _sqlConnection As MySqlConnection = New MySqlConnection
            Private _sqlQuery As MySqlCommand
            Private sqlRd As MySqlDataReader
            Private _parentConstantId As String = 0
            Private _password As String = App.Info.General.EncryptionKey
#End Region

#Region "Public Properties"

            Public Property ConnectionList As mRemoteNG.Connection.List
            Public Property ContainerList As Container.List

            Public Property SaveSecurity As Security.Save


            Public Property PropertiesVisible() As Boolean
                Get
                    If Me.btnShowProperties.Checked Then
                        Return True
                    Else
                        Return False
                    End If
                End Get
                Set(ByVal value As Boolean)
                    Me.btnShowProperties.Checked = value

                    If value = True Then
                        Me.btnShowInheritance.Checked = False
                        Me.btnShowDefaultInheritance.Checked = False
                        Me.btnShowDefaultProperties.Checked = False
                    End If
                End Set
            End Property

            Public Property InheritanceVisible() As Boolean
                Get
                    If Me.btnShowInheritance.Checked Then
                        Return True
                    Else
                        Return False
                    End If
                End Get
                Set(ByVal value As Boolean)
                    Me.btnShowInheritance.Checked = value

                    If value = True Then
                        Me.btnShowProperties.Checked = False
                        Me.btnShowDefaultInheritance.Checked = False
                        Me.btnShowDefaultProperties.Checked = False
                    End If
                End Set
            End Property

            Public Property DefaultPropertiesVisible() As Boolean
                Get
                    If Me.btnShowDefaultProperties.Checked Then
                        Return True
                    Else
                        Return False
                    End If
                End Get
                Set(ByVal value As Boolean)
                    Me.btnShowDefaultProperties.Checked = value

                    If value = True Then
                        Me.btnShowProperties.Checked = False
                        Me.btnShowDefaultInheritance.Checked = False
                        Me.btnShowInheritance.Checked = False
                    End If
                End Set
            End Property

            Public Property DefaultInheritanceVisible() As Boolean
                Get
                    If Me.btnShowDefaultInheritance.Checked Then
                        Return True
                    Else
                        Return False
                    End If
                End Get
                Set(ByVal value As Boolean)
                    Me.btnShowDefaultInheritance.Checked = value

                    If value = True Then
                        Me.btnShowProperties.Checked = False
                        Me.btnShowDefaultProperties.Checked = False
                        Me.btnShowInheritance.Checked = False
                    End If
                End Set
            End Property
#End Region

#Region "Public Methods"
            Public Sub New(ByVal Panel As DockContent)
                Me.WindowType = Type.Config
                Me.DockPnl = Panel
                Me.InitializeComponent()
            End Sub

            ' Main form handle command key events
            ' Adapted from http://kiwigis.blogspot.com/2009/05/adding-tab-key-support-to-propertygrid.html
            Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean
                If (keyData And Keys.KeyCode) = Keys.Tab Then
                    Dim selectedItem As GridItem = pGrid.SelectedGridItem
                    Dim gridRoot As GridItem = selectedItem
                    While gridRoot.GridItemType <> GridItemType.Root
                        gridRoot = gridRoot.Parent
                    End While

                    Dim gridItems As New List(Of GridItem)
                    FindChildGridItems(gridRoot, gridItems)

                    If Not ContainsGridItemProperty(gridItems) Then Return True

                    Dim newItem As GridItem = selectedItem

                    If keyData = (Keys.Tab Or Keys.Shift) Then
                        newItem = FindPreviousGridItemProperty(gridItems, selectedItem)
                    ElseIf keyData = Keys.Tab Then
                        newItem = FindNextGridItemProperty(gridItems, selectedItem)
                    End If

                    pGrid.SelectedGridItem = newItem

                    Return True ' Handled
                Else
                    Return MyBase.ProcessCmdKey(msg, keyData)
                End If
            End Function

            Private Sub FindChildGridItems(item As GridItem, ByRef gridItems As List(Of GridItem))
                gridItems.Add(item)

                If Not item.Expandable Or item.Expanded Then
                    For Each child As GridItem In item.GridItems
                        FindChildGridItems(child, gridItems)
                    Next
                End If
            End Sub

            Private Function ContainsGridItemProperty(gridItems As List(Of GridItem)) As Boolean
                For Each item As GridItem In gridItems
                    If item.GridItemType = GridItemType.Property Then Return True
                Next
                Return False
            End Function

            Private Function FindPreviousGridItemProperty(gridItems As List(Of GridItem), startItem As GridItem) As GridItem
                If gridItems.Count = 0 Then Return Nothing
                If startItem Is Nothing Then Return Nothing

                Dim startIndex As Integer = gridItems.IndexOf(startItem)

                If startItem.GridItemType = GridItemType.Property Then
                    startIndex = startIndex - 1
                    If startIndex < 0 Then startIndex = gridItems.Count - 1
                End If

                Dim previousIndex As Integer = 0
                Dim previousIndexValid As Boolean = False
                For index As Integer = startIndex To 0 Step -1
                    If gridItems(index).GridItemType = GridItemType.Property Then
                        previousIndex = index
                        previousIndexValid = True
                        Exit For
                    End If
                Next

                If previousIndexValid Then Return gridItems(previousIndex)

                For index As Integer = gridItems.Count - 1 To startIndex + 1 Step -1
                    If gridItems(index).GridItemType = GridItemType.Property Then
                        previousIndex = index
                        previousIndexValid = True
                        Exit For
                    End If
                Next

                If Not previousIndexValid Then Return Nothing

                Return gridItems(previousIndex)
            End Function

            Private Function FindNextGridItemProperty(gridItems As List(Of GridItem), startItem As GridItem) As GridItem
                If gridItems.Count = 0 Then Return Nothing
                If startItem Is Nothing Then Return Nothing

                Dim startIndex As Integer = gridItems.IndexOf(startItem)

                If startItem.GridItemType = GridItemType.Property Then
                    startIndex = startIndex + 1
                    If startIndex >= gridItems.Count Then startIndex = 0
                End If

                Dim nextIndex As Integer = 0
                Dim nextIndexValid As Boolean = False
                For index As Integer = startIndex To gridItems.Count - 1
                    If gridItems(index).GridItemType = GridItemType.Property Then
                        nextIndex = index
                        nextIndexValid = True
                        Exit For
                    End If
                Next

                If nextIndexValid Then Return gridItems(nextIndex)

                For index As Integer = 0 To startIndex - 1
                    If gridItems(index).GridItemType = GridItemType.Property Then
                        nextIndex = index
                        nextIndexValid = True
                        Exit For
                    End If
                Next

                If Not nextIndexValid Then Return Nothing

                Return gridItems(nextIndex)
            End Function

            Public Sub SetPropertyGridObject(ByVal Obj As Object)
                Try
                    Me.btnShowProperties.Enabled = False
                    Me.btnShowInheritance.Enabled = False
                    Me.btnShowDefaultProperties.Enabled = False
                    Me.btnShowDefaultInheritance.Enabled = False
                    Me.btnIcon.Enabled = False
                    Me.btnHostStatus.Enabled = False

                    Me.btnIcon.Image = Nothing

                    If TypeOf Obj Is mRemoteNG.Connection.Info Then 'CONNECTION INFO
                        If TryCast(Obj, mRemoteNG.Connection.Info).IsContainer = False Then 'NO CONTAINER
                            If Me.PropertiesVisible Then 'Properties selected
                                Me.pGrid.SelectedObject = Obj

                                Me.btnShowProperties.Enabled = True
                                If TryCast(Obj, mRemoteNG.Connection.Info).Parent IsNot Nothing Then
                                    Me.btnShowInheritance.Enabled = True
                                Else
                                    Me.btnShowInheritance.Enabled = False
                                End If
                                Me.btnShowDefaultProperties.Enabled = False
                                Me.btnShowDefaultInheritance.Enabled = False
                                btnIcon.Enabled = True
                                Me.btnHostStatus.Enabled = True
                            ElseIf Me.DefaultPropertiesVisible Then 'Defaults selected
                                Me.pGrid.SelectedObject = Obj

                                If TryCast(Obj, mRemoteNG.Connection.Info).IsDefault Then 'Is the default connection
                                    Me.btnShowProperties.Enabled = True
                                    Me.btnShowInheritance.Enabled = False
                                    Me.btnShowDefaultProperties.Enabled = True
                                    Me.btnShowDefaultInheritance.Enabled = True
                                    Me.btnIcon.Enabled = True
                                    Me.btnHostStatus.Enabled = False
                                Else 'is not the default connection
                                    Me.btnShowProperties.Enabled = True
                                    Me.btnShowInheritance.Enabled = True
                                    Me.btnShowDefaultProperties.Enabled = False
                                    Me.btnShowDefaultInheritance.Enabled = False
                                    Me.btnIcon.Enabled = True
                                    Me.btnHostStatus.Enabled = True

                                    Me.PropertiesVisible = True
                                End If
                            ElseIf Me.InheritanceVisible Then 'Inheritance selected
                                Me.pGrid.SelectedObject = TryCast(Obj, mRemoteNG.Connection.Info).Inherit

                                Me.btnShowProperties.Enabled = True
                                Me.btnShowInheritance.Enabled = True
                                Me.btnShowDefaultProperties.Enabled = False
                                Me.btnShowDefaultInheritance.Enabled = False
                                Me.btnIcon.Enabled = True
                                Me.btnHostStatus.Enabled = True
                            ElseIf Me.DefaultInheritanceVisible Then 'Default Inhertiance selected
                                pGrid.SelectedObject = Obj

                                Me.btnShowProperties.Enabled = True
                                Me.btnShowInheritance.Enabled = True
                                Me.btnShowDefaultProperties.Enabled = False
                                Me.btnShowDefaultInheritance.Enabled = False
                                Me.btnIcon.Enabled = True
                                Me.btnHostStatus.Enabled = True

                                Me.PropertiesVisible = True
                            End If
                        ElseIf TryCast(Obj, mRemoteNG.Connection.Info).IsContainer Then 'CONTAINER
                            Me.pGrid.SelectedObject = Obj

                            Me.btnShowProperties.Enabled = True
                            If TryCast(TryCast(Obj, mRemoteNG.Connection.Info).Parent, mRemoteNG.Container.Info).Parent IsNot Nothing Then
                                Me.btnShowInheritance.Enabled = True
                            Else
                                Me.btnShowInheritance.Enabled = False
                            End If
                            Me.btnShowDefaultProperties.Enabled = False
                            Me.btnShowDefaultInheritance.Enabled = False
                            Me.btnIcon.Enabled = True
                            Me.btnHostStatus.Enabled = False

                            Me.PropertiesVisible = True
                        End If

                        Dim conIcon As Icon = mRemoteNG.Connection.Icon.FromString(TryCast(Obj, mRemoteNG.Connection.Info).Icon)
                        If conIcon IsNot Nothing Then
                            Me.btnIcon.Image = conIcon.ToBitmap
                        End If
                    ElseIf TypeOf Obj Is Root.Info Then 'ROOT
                        Dim rootInfo As Root.Info = CType(Obj, Root.Info)
                        Select Case rootInfo.Type
                            Case Root.Info.RootType.Connection
                                PropertiesVisible = True
                                DefaultPropertiesVisible = False
                                btnShowProperties.Enabled = True
                                btnShowInheritance.Enabled = False
                                btnShowDefaultProperties.Enabled = True
                                btnShowDefaultInheritance.Enabled = True
                                btnIcon.Enabled = False
                                btnHostStatus.Enabled = False
                            Case Root.Info.RootType.Credential
                                Throw New NotImplementedException
                            Case Root.Info.RootType.PuttySessions
                                PropertiesVisible = True
                                DefaultPropertiesVisible = False
                                btnShowProperties.Enabled = True
                                btnShowInheritance.Enabled = False
                                btnShowDefaultProperties.Enabled = False
                                btnShowDefaultInheritance.Enabled = False
                                btnIcon.Enabled = False
                                btnHostStatus.Enabled = False
                        End Select
                        pGrid.SelectedObject = Obj
                    ElseIf TypeOf Obj Is mRemoteNG.Connection.Info.Inheritance Then 'INHERITANCE
                        Me.pGrid.SelectedObject = Obj

                        If Me.InheritanceVisible Then
                            Me.InheritanceVisible = True
                            Me.btnShowProperties.Enabled = True
                            Me.btnShowInheritance.Enabled = True
                            Me.btnShowDefaultProperties.Enabled = False
                            Me.btnShowDefaultInheritance.Enabled = False
                            Me.btnIcon.Enabled = True
                            Me.btnHostStatus.Enabled = Not TryCast(TryCast(Obj, mRemoteNG.Connection.Info.Inheritance).Parent, mRemoteNG.Connection.Info).IsContainer

                            Me.InheritanceVisible = True

                            Dim conIcon As Icon = mRemoteNG.Connection.Icon.FromString(TryCast(TryCast(Obj, mRemoteNG.Connection.Info.Inheritance).Parent, mRemoteNG.Connection.Info).Icon)
                            If conIcon IsNot Nothing Then
                                Me.btnIcon.Image = conIcon.ToBitmap
                            End If
                        ElseIf Me.DefaultInheritanceVisible Then
                            Me.btnShowProperties.Enabled = True
                            Me.btnShowInheritance.Enabled = False
                            Me.btnShowDefaultProperties.Enabled = True
                            Me.btnShowDefaultInheritance.Enabled = True
                            Me.btnIcon.Enabled = False
                            Me.btnHostStatus.Enabled = False

                            Me.DefaultInheritanceVisible = True
                        End If

                    End If

                    Me.ShowHideGridItems()
                    Me.SetHostStatus(Obj)
                Catch ex As Exception
                    MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, My.Language.strConfigPropertyGridObjectFailed & vbNewLine & ex.Message, True)
                End Try
            End Sub

            Public Sub pGrid_SelectedObjectChanged()
                Me.ShowHideGridItems()
            End Sub
#End Region

#Region "Private Methods"
            Private Sub ApplyLanguage()
                btnShowInheritance.Text = My.Language.strButtonInheritance
                btnShowDefaultInheritance.Text = My.Language.strButtonDefaultInheritance
                btnShowProperties.Text = My.Language.strButtonProperties
                btnShowDefaultProperties.Text = My.Language.strButtonDefaultProperties
                btnIcon.Text = My.Language.strButtonIcon
                btnHostStatus.Text = My.Language.strStatus
                Text = My.Language.strMenuConfig
                TabText = My.Language.strMenuConfig
                propertyGridContextMenuShowHelpText.Text = Language.strMenuShowHelpText
            End Sub

            Private Sub ApplyTheme()
                With Themes.ThemeManager.ActiveTheme
                    pGrid.BackColor = .ToolbarBackgroundColor
                    pGrid.ForeColor = .ToolbarTextColor
                    pGrid.ViewBackColor = .ConfigPanelBackgroundColor
                    pGrid.ViewForeColor = .ConfigPanelTextColor
                    pGrid.LineColor = .ConfigPanelGridLineColor
                    pGrid.HelpBackColor = .ConfigPanelHelpBackgroundColor
                    pGrid.HelpForeColor = .ConfigPanelHelpTextColor
                    pGrid.CategoryForeColor = .ConfigPanelCategoryTextColor
                End With
            End Sub

            Private _originalPropertyGridToolStripItemCountValid As Boolean
            Private _originalPropertyGridToolStripItemCount As Integer

            Private Sub AddToolStripItems()
                Try
                    Dim customToolStrip As ToolStrip = New ToolStrip
                    customToolStrip.Items.Add(btnShowProperties)
                    customToolStrip.Items.Add(btnShowInheritance)
                    customToolStrip.Items.Add(btnShowDefaultProperties)
                    customToolStrip.Items.Add(btnShowDefaultInheritance)
                    customToolStrip.Items.Add(btnHostStatus)
                    customToolStrip.Items.Add(btnIcon)
                    customToolStrip.Show()

                    Dim propertyGridToolStrip As New ToolStrip

                    Dim toolStrip As ToolStrip = Nothing
                    For Each control As System.Windows.Forms.Control In pGrid.Controls
                        toolStrip = TryCast(control, ToolStrip)

                        If toolStrip IsNot Nothing Then
                            propertyGridToolStrip = toolStrip
                            Exit For
                        End If
                    Next

                    If toolStrip Is Nothing Then
                        MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, Language.strCouldNotFindToolStripInFilteredPropertyGrid, True)
                        Return
                    End If

                    If Not _originalPropertyGridToolStripItemCountValid Then
                        _originalPropertyGridToolStripItemCount = propertyGridToolStrip.Items.Count
                        _originalPropertyGridToolStripItemCountValid = True
                    End If
                    Debug.Assert(_originalPropertyGridToolStripItemCount = 5)

                    ' Hide the "Property Pages" button
                    propertyGridToolStrip.Items(_originalPropertyGridToolStripItemCount - 1).Visible = False

                    Dim expectedToolStripItemCount As Integer = _originalPropertyGridToolStripItemCount + customToolStrip.Items.Count
                    If propertyGridToolStrip.Items.Count <> expectedToolStripItemCount Then
                        propertyGridToolStrip.AllowMerge = True
                        ToolStripManager.Merge(customToolStrip, propertyGridToolStrip)
                    End If
                Catch ex As Exception
                    MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, Language.strConfigUiLoadFailed & vbNewLine & ex.Message, True)
                End Try
            End Sub

            Private Sub Config_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                ApplyLanguage()

                AddHandler Themes.ThemeManager.ThemeChanged, AddressOf ApplyTheme
                ApplyTheme()

                AddToolStripItems()

                pGrid.HelpVisible = Settings.ShowConfigHelpText
            End Sub

            Private Sub Config_SystemColorsChanged(sender As System.Object, e As System.EventArgs) Handles MyBase.SystemColorsChanged
                AddToolStripItems()
            End Sub

            Private Sub pGrid_PropertyValueChanged(ByVal s As Object, ByVal e As System.Windows.Forms.PropertyValueChangedEventArgs) Handles pGrid.PropertyValueChanged
                Try
                    If TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Connection.Info Then
                        Select Case e.ChangedItem.Label
                            Case My.Language.strPropertyNameProtocol
                                TryCast(Me.pGrid.SelectedObject, mRemoteNG.Connection.Info).SetDefaultPort()
                            Case My.Language.strPropertyNameName
                                App.Runtime.Windows.treeForm.tvConnections.SelectedNode.Text = Me.pGrid.SelectedObject.Name
                                If My.Settings.SetHostnameLikeDisplayName And TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Connection.Info Then
                                    Dim connectionInfo As mRemoteNG.Connection.Info = DirectCast(Me.pGrid.SelectedObject, mRemoteNG.Connection.Info)
                                    If Not String.IsNullOrEmpty(connectionInfo.Name) Then
                                        connectionInfo.Hostname = connectionInfo.Name
                                    End If
                                End If
                            Case My.Language.strPropertyNameIcon
                                Dim conIcon As Icon = mRemoteNG.Connection.Icon.FromString(TryCast(Me.pGrid.SelectedObject, mRemoteNG.Connection.Info).Icon)
                                If conIcon IsNot Nothing Then
                                    Me.btnIcon.Image = conIcon.ToBitmap
                                End If
                            Case My.Language.strPropertyNameAddress
                                Me.SetHostStatus(Me.pGrid.SelectedObject)
                        End Select

                        If TryCast(Me.pGrid.SelectedObject, mRemoteNG.Connection.Info).IsDefault Then
                            App.Runtime.DefaultConnectionToSettings()
                        End If
                    End If

                    Dim rootInfo As Info = TryCast(pGrid.SelectedObject, Info)
                    If (rootInfo IsNot Nothing) Then
                        Select Case e.ChangedItem.PropertyDescriptor.Name
                            Case "Password"
                                If rootInfo.Password = True Then
                                    Dim passwordName As String
                                    If Settings.UseSQLServer Then
                                        passwordName = Language.strSQLServer.TrimEnd(":")
                                    Else
                                        passwordName = Path.GetFileName(GetStartupConnectionFileName())
                                    End If

                                    Dim password As String = Tools.Misc.PasswordDialog(passwordName)

                                    If String.IsNullOrEmpty(password) Then
                                        rootInfo.Password = False
                                    Else
                                        rootInfo.PasswordString = password
                                    End If
                                End If
                            Case "Name"
                                'Windows.treeForm.tvConnections.SelectedNode.Text = pGrid.SelectedObject.Name
                        End Select
                    End If

                    If TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Connection.Info.Inheritance Then
                        If TryCast(Me.pGrid.SelectedObject, mRemoteNG.Connection.Info.Inheritance).IsDefault Then
                            App.Runtime.DefaultInheritanceToSettings()
                        End If
                    End If



                    Dim _sqlConnection As MySqlConnection = New MySqlConnection
                    Dim _sqlQuery As MySqlCommand

                    Dim curConI As mRemoteNG.Connection.Info


                    _sqlConnection.ConnectionString = "Server=" & My.Settings.SQLHost & ";Database= " & My.Settings.SQLDatabaseName & ";Uid=" & My.Settings.SQLUser & ";Pwd=" & Security.Crypt.Decrypt(My.Settings.SQLPass, App.Info.General.EncryptionKey) & ";"


                    _sqlConnection.Open()



                    Dim saveSecurity As New Security.Save()
                    Dim _parentConstantId As String = 0
                    Dim _password As String = App.Info.General.EncryptionKey

                    curConI = Me.pGrid.SelectedObject



                    _sqlQuery = New MySqlCommand("UPDATE tblCons SET ", _sqlConnection)

                    If mRemoteNG.Tree.Node.GetNodeType(curConI.TreeNode) = mRemoteNG.Tree.Node.Type.Connection Or mRemoteNG.Tree.Node.GetNodeType(curConI.TreeNode) = mRemoteNG.Tree.Node.Type.Container Then
                        '_xmlTextWriter.WriteStartElement("Node")
                        _sqlQuery.CommandText &= "Name='" & PrepareValueForDB(curConI.TreeNode.Text) & "'," 'Name
                        _sqlQuery.CommandText &= "Type='" & mRemoteNG.Tree.Node.GetNodeType(curConI.TreeNode).ToString & "'," 'Type
                    End If

                    If mRemoteNG.Tree.Node.GetNodeType(curConI.TreeNode) = mRemoteNG.Tree.Node.Type.Container Then 'container
                        _sqlQuery.CommandText &= "Expanded='" & ContainerList(curConI.TreeNode.Tag).IsExpanded & "'," 'Expanded
                        curConI.PositionID = curConI.TreeNode.Index + 1
                    End If

                    If mRemoteNG.Tree.Node.GetNodeType(curConI.TreeNode) = mRemoteNG.Tree.Node.Type.Connection Then
                        _sqlQuery.CommandText &= "Expanded='" & False & "',"
                        curConI.PositionID = curConI.TreeNode.Index + 1
                    End If

                    With curConI
                        _sqlQuery.CommandText &= "Description='" & PrepareValueForDB(.Description) & "',"
                        _sqlQuery.CommandText &= "Icon='" & PrepareValueForDB(.Icon) & "',"
                        _sqlQuery.CommandText &= "Panel='" & PrepareValueForDB(.Panel) & "',"

                        If saveSecurity.Username = True Then
                            _sqlQuery.CommandText &= "Username='" & PrepareValueForDB(.Username) & "',"
                        Else
                            _sqlQuery.CommandText &= "Username='" & "" & "',"
                        End If

                        If saveSecurity.Domain = True Then
                            _sqlQuery.CommandText &= "DomainName='" & PrepareValueForDB(.Domain) & "',"
                        Else
                            _sqlQuery.CommandText &= "DomainName='" & "" & "',"
                        End If

                        If saveSecurity.Password = True Then
                            _sqlQuery.CommandText &= "Password='" & PrepareValueForDB(Security.Crypt.Encrypt(.Password, _password)) & "',"
                        Else
                            _sqlQuery.CommandText &= "Password='" & "" & "',"
                        End If

                        _sqlQuery.CommandText &= "Hostname='" & PrepareValueForDB(.Hostname) & "',"
                        _sqlQuery.CommandText &= "Protocol='" & .Protocol.ToString & "',"
                        _sqlQuery.CommandText &= "PuttySession='" & PrepareValueForDB(.PuttySession) & "',"
                        _sqlQuery.CommandText &= "Port='" & .Port & "',"
                        _sqlQuery.CommandText &= "ConnectToConsole='" & .UseConsoleSession & "',"
                        _sqlQuery.CommandText &= "RenderingEngine='" & .RenderingEngine.ToString & "',"
                        _sqlQuery.CommandText &= "ICAEncryptionStrength='" & .ICAEncryption.ToString & "',"
                        _sqlQuery.CommandText &= "RDPAuthenticationLevel='" & .RDPAuthenticationLevel.ToString & "',"
                        _sqlQuery.CommandText &= "LoadBalanceInfo='" & .LoadBalanceInfo & "',"
                        _sqlQuery.CommandText &= "Colors='" & .Colors.ToString & "',"
                        _sqlQuery.CommandText &= "Resolution='" & .Resolution.ToString & "',"
                        _sqlQuery.CommandText &= "AutomaticResize='" & .AutomaticResize & "',"
                        _sqlQuery.CommandText &= "DisplayWallpaper='" & .DisplayWallpaper & "',"
                        _sqlQuery.CommandText &= "DisplayThemes='" & .DisplayThemes & "',"
                        _sqlQuery.CommandText &= "EnableFontSmoothing='" & .EnableFontSmoothing & "',"
                        _sqlQuery.CommandText &= "EnableDesktopComposition='" & .EnableDesktopComposition & "',"
                        _sqlQuery.CommandText &= "CacheBitmaps='" & .CacheBitmaps & "',"
                        _sqlQuery.CommandText &= "RedirectDiskDrives='" & .RedirectDiskDrives & "',"
                        _sqlQuery.CommandText &= "RedirectPorts='" & .RedirectPorts & "',"
                        _sqlQuery.CommandText &= "RedirectPrinters='" & .RedirectPrinters & "',"
                        _sqlQuery.CommandText &= "RedirectSmartCards='" & .RedirectSmartCards & "',"
                        _sqlQuery.CommandText &= "RedirectSound='" & .RedirectSound.ToString & "',"
                        _sqlQuery.CommandText &= "RedirectKeys='" & .RedirectKeys & "',"

                        If curConI.OpenConnections.Count > 0 Then
                            _sqlQuery.CommandText &= "Connected=" & 1 & ","
                        Else
                            _sqlQuery.CommandText &= "Connected=" & 0 & ","
                        End If

                        _sqlQuery.CommandText &= "PreExtApp='" & .PreExtApp & "',"
                        _sqlQuery.CommandText &= "PostExtApp='" & .PostExtApp & "',"
                        _sqlQuery.CommandText &= "MacAddress='" & .MacAddress & "',"
                        _sqlQuery.CommandText &= "UserField='" & .UserField & "',"
                        _sqlQuery.CommandText &= "ExtApp='" & .ExtApp & "',"

                        _sqlQuery.CommandText &= "VNCCompression='" & .VNCCompression.ToString & "',"
                        _sqlQuery.CommandText &= "VNCEncoding='" & .VNCEncoding.ToString & "',"
                        _sqlQuery.CommandText &= "VNCAuthMode='" & .VNCAuthMode.ToString & "',"
                        _sqlQuery.CommandText &= "VNCProxyType='" & .VNCProxyType.ToString & "',"
                        _sqlQuery.CommandText &= "VNCProxyIP='" & .VNCProxyIP & "',"
                        _sqlQuery.CommandText &= "VNCProxyPort='" & .VNCProxyPort & "',"
                        _sqlQuery.CommandText &= "VNCProxyUsername='" & .VNCProxyUsername & "',"
                        _sqlQuery.CommandText &= "VNCProxyPassword='" & Security.Crypt.Encrypt(.VNCProxyPassword, _password) & "',"
                        _sqlQuery.CommandText &= "VNCColors='" & .VNCColors.ToString & "',"
                        _sqlQuery.CommandText &= "VNCSmartSizeMode='" & .VNCSmartSizeMode.ToString & "',"
                        _sqlQuery.CommandText &= "VNCViewOnly='" & .VNCViewOnly & "',"

                        _sqlQuery.CommandText &= "RDGatewayUsageMethod='" & .RDGatewayUsageMethod.ToString & "',"
                        _sqlQuery.CommandText &= "RDGatewayHostname='" & .RDGatewayHostname & "',"
                        _sqlQuery.CommandText &= "RDGatewayUseConnectionCredentials='" & .RDGatewayUseConnectionCredentials.ToString & "',"

                        If saveSecurity.Username = True Then
                            _sqlQuery.CommandText &= "RDGatewayUsername='" & .RDGatewayUsername & "',"
                        Else
                            _sqlQuery.CommandText &= "RDGatewayUsername='" & "" & "',"
                        End If

                        If saveSecurity.Password = True Then
                            _sqlQuery.CommandText &= "RDGatewayPassword='" & Security.Crypt.Encrypt(.RDGatewayPassword, _password) & "',"
                        Else
                            _sqlQuery.CommandText &= "RDGatewayPassword='" & "" & "',"
                        End If

                        If saveSecurity.Domain = True Then
                            _sqlQuery.CommandText &= "RDGatewayDomain='" & .RDGatewayDomain & "',"
                        Else
                            _sqlQuery.CommandText &= "RDGatewayDomain='" & "" & "',"
                        End If

                        _sqlQuery.CommandText &= "UseCredSsp='" & .UseCredSsp & "',"

                        With .Inherit
                            If saveSecurity.Inheritance = True Then
                                _sqlQuery.CommandText &= "InheritCacheBitmaps='" & .CacheBitmaps & "',"
                                _sqlQuery.CommandText &= "InheritColors='" & .Colors & "',"
                                _sqlQuery.CommandText &= "InheritDescription='" & .Description & "',"
                                _sqlQuery.CommandText &= "InheritDisplayThemes='" & .DisplayThemes & "',"
                                _sqlQuery.CommandText &= "InheritDisplayWallpaper='" & .DisplayWallpaper & "',"
                                _sqlQuery.CommandText &= "InheritEnableFontSmoothing='" & .EnableFontSmoothing & "',"
                                _sqlQuery.CommandText &= "InheritEnableDesktopComposition='" & .EnableDesktopComposition & "',"
                                _sqlQuery.CommandText &= "InheritDomain='" & .Domain & "',"
                                _sqlQuery.CommandText &= "InheritIcon='" & .Icon & "',"
                                _sqlQuery.CommandText &= "InheritPanel='" & .Panel & "',"
                                _sqlQuery.CommandText &= "InheritPassword='" & .Password & "',"
                                _sqlQuery.CommandText &= "InheritPort='" & .Port & "',"
                                _sqlQuery.CommandText &= "InheritProtocol='" & .Protocol & "',"
                                _sqlQuery.CommandText &= "InheritPuttySession='" & .PuttySession & "',"
                                _sqlQuery.CommandText &= "InheritRedirectDiskDrives='" & .RedirectDiskDrives & "',"
                                _sqlQuery.CommandText &= "InheritRedirectKeys='" & .RedirectKeys & "',"
                                _sqlQuery.CommandText &= "InheritRedirectPorts='" & .RedirectPorts & "',"
                                _sqlQuery.CommandText &= "InheritRedirectPrinters='" & .RedirectPrinters & "',"
                                _sqlQuery.CommandText &= "InheritRedirectSmartCards='" & .RedirectSmartCards & "',"
                                _sqlQuery.CommandText &= "InheritRedirectSound='" & .RedirectSound & "',"
                                _sqlQuery.CommandText &= "InheritResolution='" & .Resolution & "',"
                                _sqlQuery.CommandText &= "InheritAutomaticResize='" & .AutomaticResize & "',"
                                _sqlQuery.CommandText &= "InheritUseConsoleSession='" & .UseConsoleSession & "',"
                                _sqlQuery.CommandText &= "InheritRenderingEngine='" & .RenderingEngine & "',"
                                _sqlQuery.CommandText &= "InheritUsername='" & .Username & "',"
                                _sqlQuery.CommandText &= "InheritICAEncryptionStrength='" & .ICAEncryption & "',"
                                _sqlQuery.CommandText &= "InheritRDPAuthenticationLevel='" & .RDPAuthenticationLevel & "',"
                                _sqlQuery.CommandText &= "InheritLoadBalanceInfo='" & .LoadBalanceInfo & "',"
                                _sqlQuery.CommandText &= "InheritPreExtApp='" & .PreExtApp & "',"
                                _sqlQuery.CommandText &= "InheritPostExtApp='" & .PostExtApp & "',"
                                _sqlQuery.CommandText &= "InheritMacAddress='" & .MacAddress & "',"
                                _sqlQuery.CommandText &= "InheritUserField='" & .UserField & "',"
                                _sqlQuery.CommandText &= "InheritExtApp='" & .ExtApp & "',"

                                _sqlQuery.CommandText &= "InheritVNCCompression='" & .VNCCompression & "',"
                                _sqlQuery.CommandText &= "InheritVNCEncoding='" & .VNCEncoding & "',"
                                _sqlQuery.CommandText &= "InheritVNCAuthMode='" & .VNCAuthMode & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyType='" & .VNCProxyType & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyIP='" & .VNCProxyIP & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyPort='" & .VNCProxyPort & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyUsername='" & .VNCProxyUsername & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyPassword='" & .VNCProxyPassword & "',"
                                _sqlQuery.CommandText &= "InheritVNCColors='" & .VNCColors & "',"
                                _sqlQuery.CommandText &= "InheritVNCSmartSizeMode='" & .VNCSmartSizeMode & "',"
                                _sqlQuery.CommandText &= "InheritVNCViewOnly='" & .VNCViewOnly & "',"

                                _sqlQuery.CommandText &= "InheritRDGatewayUsageMethod='" & .RDGatewayUsageMethod & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayHostname='" & .RDGatewayHostname & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayUseConnectionCredentials='" & .RDGatewayUseConnectionCredentials & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayUsername='" & .RDGatewayUsername & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayPassword='" & .RDGatewayPassword & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayDomain='" & .RDGatewayDomain & "',"

                                _sqlQuery.CommandText &= "InheritUseCredSsp='" & .UseCredSsp & "',"
                            Else
                                _sqlQuery.CommandText &= "InheritCacheBitmaps='" & False & "',"
                                _sqlQuery.CommandText &= "InheritColors='" & False & "',"
                                _sqlQuery.CommandText &= "InheritDescription='" & False & "',"
                                _sqlQuery.CommandText &= "InheritDisplayThemes='" & False & "',"
                                _sqlQuery.CommandText &= "InheritDisplayWallpaper='" & False & "',"
                                _sqlQuery.CommandText &= "InheritEnableFontSmoothing='" & False & "',"
                                _sqlQuery.CommandText &= "InheritEnableDesktopComposition='" & False & "',"
                                _sqlQuery.CommandText &= "InheritDomain='" & False & "',"
                                _sqlQuery.CommandText &= "InheritIcon='" & False & "',"
                                _sqlQuery.CommandText &= "InheritPanel='" & False & "',"
                                _sqlQuery.CommandText &= "InheritPassword='" & False & "',"
                                _sqlQuery.CommandText &= "InheritPort='" & False & "',"
                                _sqlQuery.CommandText &= "InheritProtocol='" & False & "',"
                                _sqlQuery.CommandText &= "InheritPuttySession='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRedirectDiskDrives='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRedirectKeys='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRedirectPorts='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRedirectPrinters='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRedirectSmartCards='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRedirectSound='" & False & "',"
                                _sqlQuery.CommandText &= "InheritResolution='" & False & "',"
                                _sqlQuery.CommandText &= "InheritAutomaticResize='" & False & "',"
                                _sqlQuery.CommandText &= "InheritUseConsoleSession='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRenderingEngine='" & False & "',"
                                _sqlQuery.CommandText &= "InheritUsername='" & False & "',"
                                _sqlQuery.CommandText &= "InheritICAEncryptionStrength='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRDPAuthenticationLevel='" & False & "',"
                                _sqlQuery.CommandText &= "InheritLoadBalanceInfo='" & False & "',"
                                _sqlQuery.CommandText &= "InheritPreExtApp='" & False & "',"
                                _sqlQuery.CommandText &= "InheritPostExtApp='" & False & "',"
                                _sqlQuery.CommandText &= "InheritMacAddress='" & False & "',"
                                _sqlQuery.CommandText &= "InheritUserField='" & False & "',"
                                _sqlQuery.CommandText &= "InheritExtApp='" & False & "',"

                                _sqlQuery.CommandText &= "InheritVNCCompression='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCEncoding='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCAuthMode='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyType='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyIP='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyPort='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyUsername='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCProxyPassword='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCColors='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCSmartSizeMode='" & False & "',"
                                _sqlQuery.CommandText &= "InheritVNCViewOnly='" & False & "',"

                                _sqlQuery.CommandText &= "InheritRDGatewayUsageMethod='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayHostname='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayUseConnectionCredentials='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayUsername='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayPassword='" & False & "',"
                                _sqlQuery.CommandText &= "InheritRDGatewayDomain='" & False & "',"

                                _sqlQuery.CommandText &= "InheritUseCredSsp='" & False & "',"
                            End If
                        End With

                        'TODO rendre propre double if...
                        ' If curConI.Parent Is Nothing Then
                        ' _sqlQuery.CommandText &= "PositionID='" & .PositionID & "',"
                        ' Else
                        'If curConI.Parent.ConnectionInfo Is Nothing Then
                        '_sqlQuery.CommandText &= "PositionID='" & .PositionID & "',"
                        'Else
                        '_sqlQuery.CommandText &= "PositionID='" & .PositionID + 1 + curConI.Parent.ConnectionInfo.PositionID & "',"
                        'End If
                        '
                        'End If

                        If .IsContainer = False Then
                            If .Parent IsNot Nothing Then
                                _parentConstantId = TryCast(.Parent, Container.Info).ConnectionInfo.ConstantID
                            Else
                                _parentConstantId = 0
                            End If
                        Else
                            If TryCast(.Parent, Container.Info).Parent IsNot Nothing Then
                                _parentConstantId = TryCast(TryCast(.Parent, Container.Info).Parent, Container.Info).ConnectionInfo.ConstantID
                            Else
                                _parentConstantId = 0
                            End If
                        End If
                        _sqlQuery.CommandText &= "ParentID='" & _parentConstantId & "',ConstantID='" & .ConstantID & "',LastChange='" & Tools.Misc.DBDate(Now) & "'"
                    End With

                    _sqlQuery.CommandText &= " WHERE ConstantID='" & curConI.ConstantID & "'"


                    _sqlQuery.CommandText = Tools.Misc.PrepareForDB(_sqlQuery.CommandText)
                    _sqlQuery.ExecuteNonQuery()

                    _sqlQuery = New MySqlCommand("UPDATE tblUpdate SET LastUpdate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'", _sqlConnection)
                    _sqlQuery.ExecuteNonQuery()
                    _sqlConnection.Close()


                    Me.ShowHideGridItems()
                    'App.Runtime.SaveConnectionsBG()
                Catch ex As Exception
                    MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, My.Language.strConfigPropertyGridValueFailed & vbNewLine & ex.Message, True)
                End Try
            End Sub

            Private Sub SaveNodesSQL(node As TreeNode)


                Dim curConI As mRemoteNG.Connection.Info
                _sqlQuery = New MySqlCommand("INSERT INTO tblCons (Name, Type, Expanded, Description, Icon, Panel, Username, " &
                                               "DomainName, Password, Hostname, Protocol, PuttySession, " &
                                               "Port, ConnectToConsole, RenderingEngine, ICAEncryptionStrength, RDPAuthenticationLevel, LoadBalanceInfo, Colors, Resolution, AutomaticResize, DisplayWallpaper, " &
                                               "DisplayThemes, EnableFontSmoothing, EnableDesktopComposition, CacheBitmaps, RedirectDiskDrives, RedirectPorts, " &
                                               "RedirectPrinters, RedirectSmartCards, RedirectSound, RedirectKeys, " &
                                               "Connected, PreExtApp, PostExtApp, MacAddress, UserField, ExtApp, VNCCompression, VNCEncoding, VNCAuthMode, " &
                                               "VNCProxyType, VNCProxyIP, VNCProxyPort, VNCProxyUsername, VNCProxyPassword, " &
                                               "VNCColors, VNCSmartSizeMode, VNCViewOnly, " &
                                               "RDGatewayUsageMethod, RDGatewayHostname, RDGatewayUseConnectionCredentials, RDGatewayUsername, RDGatewayPassword, RDGatewayDomain, " &
                                               "UseCredSsp, " &
                                               "InheritCacheBitmaps, InheritColors, " &
                                               "InheritDescription, InheritDisplayThemes, InheritDisplayWallpaper, InheritEnableFontSmoothing, InheritEnableDesktopComposition, InheritDomain, " &
                                               "InheritIcon, InheritPanel, InheritPassword, InheritPort, " &
                                               "InheritProtocol, InheritPuttySession, InheritRedirectDiskDrives, " &
                                               "InheritRedirectKeys, InheritRedirectPorts, InheritRedirectPrinters, " &
                                               "InheritRedirectSmartCards, InheritRedirectSound, InheritResolution, InheritAutomaticResize, " &
                                               "InheritUseConsoleSession, InheritRenderingEngine, InheritUsername, InheritICAEncryptionStrength, InheritRDPAuthenticationLevel, InheritLoadBalanceInfo, " &
                                               "InheritPreExtApp, InheritPostExtApp, InheritMacAddress, InheritUserField, InheritExtApp, InheritVNCCompression, InheritVNCEncoding, " &
                                               "InheritVNCAuthMode, InheritVNCProxyType, InheritVNCProxyIP, InheritVNCProxyPort, " &
                                               "InheritVNCProxyUsername, InheritVNCProxyPassword, InheritVNCColors, " &
                                               "InheritVNCSmartSizeMode, InheritVNCViewOnly, " &
                                               "InheritRDGatewayUsageMethod, InheritRDGatewayHostname, InheritRDGatewayUseConnectionCredentials, InheritRDGatewayUsername, InheritRDGatewayPassword, InheritRDGatewayDomain, " &
                                               "InheritUseCredSsp, " &
                                               "PositionID, ParentID, ConstantID, LastChange)" &
                                               "VALUES (", _sqlConnection)

                If mRemoteNG.Tree.Node.GetNodeType(node) = mRemoteNG.Tree.Node.Type.Connection Or mRemoteNG.Tree.Node.GetNodeType(node) = mRemoteNG.Tree.Node.Type.Container Then
                    '_xmlTextWriter.WriteStartElement("Node")
                    _sqlQuery.CommandText &= "'" & PrepareValueForDB(node.Text) & "'," 'Name
                    _sqlQuery.CommandText &= "'" & mRemoteNG.Tree.Node.GetNodeType(node).ToString & "'," 'Type
                End If

                If mRemoteNG.Tree.Node.GetNodeType(node) = mRemoteNG.Tree.Node.Type.Container Then 'container
                    _sqlQuery.CommandText &= "'" & Me._ContainerList(node.Tag).IsExpanded & "'," 'Expanded
                    curConI = Me._ContainerList(node.Tag).ConnectionInfo
                    SaveConnectionFieldsSQL(curConI)

                    _sqlQuery.CommandText = Tools.Misc.PrepareForDB(_sqlQuery.CommandText)
                    _sqlQuery.ExecuteNonQuery()
                    '_parentConstantId = _currentNodeIndex
                    SaveNodesSQL(node)
                    '_xmlTextWriter.WriteEndElement()
                End If

                If mRemoteNG.Tree.Node.GetNodeType(node) = mRemoteNG.Tree.Node.Type.Connection Then
                    _sqlQuery.CommandText &= "'" & False & "',"
                    curConI = Me._ConnectionList(node.Tag)
                    SaveConnectionFieldsSQL(curConI)
                    '_xmlTextWriter.WriteEndElement()
                    _sqlQuery.CommandText = Tools.Misc.PrepareForDB(_sqlQuery.CommandText)
                    _sqlQuery.ExecuteNonQuery()
                End If

            End Sub

            Private Sub pGrid_PropertySortChanged(ByVal sender As Object, ByVal e As EventArgs) Handles pGrid.PropertySortChanged
                If pGrid.PropertySort = PropertySort.CategorizedAlphabetical Then
                    pGrid.PropertySort = PropertySort.Categorized
                End If
            End Sub

            Private Sub SaveConnectionFieldsSQL(ByVal curConI As mRemoteNG.Connection.Info)
                With curConI
                    _sqlQuery.CommandText &= "'" & PrepareValueForDB(.Description) & "',"
                    _sqlQuery.CommandText &= "'" & PrepareValueForDB(.Icon) & "',"
                    _sqlQuery.CommandText &= "'" & PrepareValueForDB(.Panel) & "',"

                    If Me._SaveSecurity.Username = True Then
                        _sqlQuery.CommandText &= "'" & PrepareValueForDB(.Username) & "',"
                    Else
                        _sqlQuery.CommandText &= "'" & "" & "',"
                    End If

                    If Me._SaveSecurity.Domain = True Then
                        _sqlQuery.CommandText &= "'" & PrepareValueForDB(.Domain) & "',"
                    Else
                        _sqlQuery.CommandText &= "'" & "" & "',"
                    End If

                    If Me._SaveSecurity.Password = True Then
                        _sqlQuery.CommandText &= "'" & PrepareValueForDB(Security.Crypt.Encrypt(.Password, _password)) & "',"
                    Else
                        _sqlQuery.CommandText &= "'" & "" & "',"
                    End If

                    _sqlQuery.CommandText &= "'" & PrepareValueForDB(.Hostname) & "',"
                    _sqlQuery.CommandText &= "'" & .Protocol.ToString & "',"
                    _sqlQuery.CommandText &= "'" & PrepareValueForDB(.PuttySession) & "',"
                    _sqlQuery.CommandText &= "'" & .Port & "',"
                    _sqlQuery.CommandText &= "'" & .UseConsoleSession & "',"
                    _sqlQuery.CommandText &= "'" & .RenderingEngine.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .ICAEncryption.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .RDPAuthenticationLevel.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .LoadBalanceInfo & "',"
                    _sqlQuery.CommandText &= "'" & .Colors.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .Resolution.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .AutomaticResize & "',"
                    _sqlQuery.CommandText &= "'" & .DisplayWallpaper & "',"
                    _sqlQuery.CommandText &= "'" & .DisplayThemes & "',"
                    _sqlQuery.CommandText &= "'" & .EnableFontSmoothing & "',"
                    _sqlQuery.CommandText &= "'" & .EnableDesktopComposition & "',"
                    _sqlQuery.CommandText &= "'" & .CacheBitmaps & "',"
                    _sqlQuery.CommandText &= "'" & .RedirectDiskDrives & "',"
                    _sqlQuery.CommandText &= "'" & .RedirectPorts & "',"
                    _sqlQuery.CommandText &= "'" & .RedirectPrinters & "',"
                    _sqlQuery.CommandText &= "'" & .RedirectSmartCards & "',"
                    _sqlQuery.CommandText &= "'" & .RedirectSound.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .RedirectKeys & "',"

                    If curConI.OpenConnections.Count > 0 Then
                        _sqlQuery.CommandText &= 1 & ","
                    Else
                        _sqlQuery.CommandText &= 0 & ","
                    End If

                    _sqlQuery.CommandText &= "'" & .PreExtApp & "',"
                    _sqlQuery.CommandText &= "'" & .PostExtApp & "',"
                    _sqlQuery.CommandText &= "'" & .MacAddress & "',"
                    _sqlQuery.CommandText &= "'" & .UserField & "',"
                    _sqlQuery.CommandText &= "'" & .ExtApp & "',"

                    _sqlQuery.CommandText &= "'" & .VNCCompression.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .VNCEncoding.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .VNCAuthMode.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .VNCProxyType.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .VNCProxyIP & "',"
                    _sqlQuery.CommandText &= "'" & .VNCProxyPort & "',"
                    _sqlQuery.CommandText &= "'" & .VNCProxyUsername & "',"
                    _sqlQuery.CommandText &= "'" & Security.Crypt.Encrypt(.VNCProxyPassword, _password) & "',"
                    _sqlQuery.CommandText &= "'" & .VNCColors.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .VNCSmartSizeMode.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .VNCViewOnly & "',"

                    _sqlQuery.CommandText &= "'" & .RDGatewayUsageMethod.ToString & "',"
                    _sqlQuery.CommandText &= "'" & .RDGatewayHostname & "',"
                    _sqlQuery.CommandText &= "'" & .RDGatewayUseConnectionCredentials.ToString & "',"

                    If Me._SaveSecurity.Username = True Then
                        _sqlQuery.CommandText &= "'" & .RDGatewayUsername & "',"
                    Else
                        _sqlQuery.CommandText &= "'" & "" & "',"
                    End If

                    If Me._SaveSecurity.Password = True Then
                        _sqlQuery.CommandText &= "'" & Security.Crypt.Encrypt(.RDGatewayPassword, _password) & "',"
                    Else
                        _sqlQuery.CommandText &= "'" & "" & "',"
                    End If

                    If Me._SaveSecurity.Domain = True Then
                        _sqlQuery.CommandText &= "'" & .RDGatewayDomain & "',"
                    Else
                        _sqlQuery.CommandText &= "'" & "" & "',"
                    End If

                    _sqlQuery.CommandText &= "'" & .UseCredSsp & "',"

                    With .Inherit
                        If Me._SaveSecurity.Inheritance = True Then
                            _sqlQuery.CommandText &= "'" & .CacheBitmaps & "',"
                            _sqlQuery.CommandText &= "'" & .Colors & "',"
                            _sqlQuery.CommandText &= "'" & .Description & "',"
                            _sqlQuery.CommandText &= "'" & .DisplayThemes & "',"
                            _sqlQuery.CommandText &= "'" & .DisplayWallpaper & "',"
                            _sqlQuery.CommandText &= "'" & .EnableFontSmoothing & "',"
                            _sqlQuery.CommandText &= "'" & .EnableDesktopComposition & "',"
                            _sqlQuery.CommandText &= "'" & .Domain & "',"
                            _sqlQuery.CommandText &= "'" & .Icon & "',"
                            _sqlQuery.CommandText &= "'" & .Panel & "',"
                            _sqlQuery.CommandText &= "'" & .Password & "',"
                            _sqlQuery.CommandText &= "'" & .Port & "',"
                            _sqlQuery.CommandText &= "'" & .Protocol & "',"
                            _sqlQuery.CommandText &= "'" & .PuttySession & "',"
                            _sqlQuery.CommandText &= "'" & .RedirectDiskDrives & "',"
                            _sqlQuery.CommandText &= "'" & .RedirectKeys & "',"
                            _sqlQuery.CommandText &= "'" & .RedirectPorts & "',"
                            _sqlQuery.CommandText &= "'" & .RedirectPrinters & "',"
                            _sqlQuery.CommandText &= "'" & .RedirectSmartCards & "',"
                            _sqlQuery.CommandText &= "'" & .RedirectSound & "',"
                            _sqlQuery.CommandText &= "'" & .Resolution & "',"
                            _sqlQuery.CommandText &= "'" & .AutomaticResize & "',"
                            _sqlQuery.CommandText &= "'" & .UseConsoleSession & "',"
                            _sqlQuery.CommandText &= "'" & .RenderingEngine & "',"
                            _sqlQuery.CommandText &= "'" & .Username & "',"
                            _sqlQuery.CommandText &= "'" & .ICAEncryption & "',"
                            _sqlQuery.CommandText &= "'" & .RDPAuthenticationLevel & "',"
                            _sqlQuery.CommandText &= "'" & .LoadBalanceInfo & "',"
                            _sqlQuery.CommandText &= "'" & .PreExtApp & "',"
                            _sqlQuery.CommandText &= "'" & .PostExtApp & "',"
                            _sqlQuery.CommandText &= "'" & .MacAddress & "',"
                            _sqlQuery.CommandText &= "'" & .UserField & "',"
                            _sqlQuery.CommandText &= "'" & .ExtApp & "',"

                            _sqlQuery.CommandText &= "'" & .VNCCompression & "',"
                            _sqlQuery.CommandText &= "'" & .VNCEncoding & "',"
                            _sqlQuery.CommandText &= "'" & .VNCAuthMode & "',"
                            _sqlQuery.CommandText &= "'" & .VNCProxyType & "',"
                            _sqlQuery.CommandText &= "'" & .VNCProxyIP & "',"
                            _sqlQuery.CommandText &= "'" & .VNCProxyPort & "',"
                            _sqlQuery.CommandText &= "'" & .VNCProxyUsername & "',"
                            _sqlQuery.CommandText &= "'" & .VNCProxyPassword & "',"
                            _sqlQuery.CommandText &= "'" & .VNCColors & "',"
                            _sqlQuery.CommandText &= "'" & .VNCSmartSizeMode & "',"
                            _sqlQuery.CommandText &= "'" & .VNCViewOnly & "',"

                            _sqlQuery.CommandText &= "'" & .RDGatewayUsageMethod & "',"
                            _sqlQuery.CommandText &= "'" & .RDGatewayHostname & "',"
                            _sqlQuery.CommandText &= "'" & .RDGatewayUseConnectionCredentials & "',"
                            _sqlQuery.CommandText &= "'" & .RDGatewayUsername & "',"
                            _sqlQuery.CommandText &= "'" & .RDGatewayPassword & "',"
                            _sqlQuery.CommandText &= "'" & .RDGatewayDomain & "',"

                            _sqlQuery.CommandText &= "'" & .UseCredSsp & "',"
                        Else
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "'," ' .AutomaticResize
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "'," ' .LoadBalanceInfo
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"

                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"
                            _sqlQuery.CommandText &= "'" & False & "',"

                            _sqlQuery.CommandText &= "'" & False & "'," ' .RDGatewayUsageMethod
                            _sqlQuery.CommandText &= "'" & False & "'," ' .RDGatewayHostname
                            _sqlQuery.CommandText &= "'" & False & "'," ' .RDGatewayUseConnectionCredentials
                            _sqlQuery.CommandText &= "'" & False & "'," ' .RDGatewayUsername
                            _sqlQuery.CommandText &= "'" & False & "'," ' .RDGatewayPassword
                            _sqlQuery.CommandText &= "'" & False & "'," ' .RDGatewayDomain

                            _sqlQuery.CommandText &= "'" & False & "'," ' .UseCredSsp
                        End If
                    End With

                    .PositionID = .PositionID

                    If .IsContainer = False Then
                        If .Parent IsNot Nothing Then
                            _parentConstantId = TryCast(.Parent, Container.Info).ConnectionInfo.ConstantID
                        Else
                            _parentConstantId = 0
                        End If
                    Else
                        If TryCast(.Parent, Container.Info).Parent IsNot Nothing Then
                            _parentConstantId = TryCast(TryCast(.Parent, Container.Info).Parent, Container.Info).ConnectionInfo.ConstantID
                        Else
                            _parentConstantId = 0
                        End If
                    End If

                    _sqlQuery.CommandText &= .PositionID & ",'" & _parentConstantId & "','" & .ConstantID & "','" & Tools.Misc.DBDate(Now) & "')"
                End With
            End Sub

            Private Sub ShowHideGridItems()
                Try
                    Dim strHide As New List(Of String)

                    If TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Connection.Info Then
                        Dim conI As mRemoteNG.Connection.Info = pGrid.SelectedObject

                        Select Case conI.Protocol
                            Case mRemoteNG.Connection.Protocol.Protocols.RDP
                                strHide.Add("ExtApp")
                                strHide.Add("ICAEncryption")
                                strHide.Add("PuttySession")
                                strHide.Add("RenderingEngine")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                                If conI.RDGatewayUsageMethod = mRemoteNG.Connection.Protocol.RDP.RDGatewayUsageMethod.Never Then
                                    strHide.Add("RDGatewayDomain")
                                    strHide.Add("RDGatewayHostname")
                                    strHide.Add("RDGatewayPassword")
                                    strHide.Add("RDGatewayUseConnectionCredentials")
                                    strHide.Add("RDGatewayUsername")
                                ElseIf conI.RDGatewayUseConnectionCredentials Then
                                    strHide.Add("RDGatewayDomain")
                                    strHide.Add("RDGatewayPassword")
                                    strHide.Add("RDGatewayUsername")
                                End If
                                If Not (conI.Resolution = RDP.RDPResolutions.FitToWindow Or _
                                        conI.Resolution = RDP.RDPResolutions.Fullscreen) Then
                                    strHide.Add("AutomaticResize")
                                End If
                            Case mRemoteNG.Connection.Protocol.Protocols.VNC
                                strHide.Add("CacheBitmaps")
                                strHide.Add("Colors")
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("ExtApp")
                                strHide.Add("ICAEncryption")
                                strHide.Add("PuttySession")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound")
                                strHide.Add("RenderingEngine")
                                strHide.Add("Resolution")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                If conI.VNCAuthMode = mRemoteNG.Connection.Protocol.VNC.AuthMode.AuthVNC Then
                                    strHide.Add("Username")
                                    strHide.Add("Domain")
                                End If
                                If conI.VNCProxyType = mRemoteNG.Connection.Protocol.VNC.ProxyType.ProxyNone Then
                                    strHide.Add("VNCProxyIP")
                                    strHide.Add("VNCProxyPassword")
                                    strHide.Add("VNCProxyPort")
                                    strHide.Add("VNCProxyUsername")
                                End If
                            Case mRemoteNG.Connection.Protocol.Protocols.SSH1
                                strHide.Add("CacheBitmaps")
                                strHide.Add("Colors")
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("Domain")
                                strHide.Add("ExtApp")
                                strHide.Add("ICAEncryption")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound")
                                strHide.Add("RenderingEngine")
                                strHide.Add("Resolution")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                            Case mRemoteNG.Connection.Protocol.Protocols.SSH2
                                strHide.Add("CacheBitmaps")
                                strHide.Add("Colors")
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("Domain")
                                strHide.Add("ExtApp")
                                strHide.Add("ICAEncryption")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound")
                                strHide.Add("RenderingEngine")
                                strHide.Add("Resolution")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                            Case mRemoteNG.Connection.Protocol.Protocols.Telnet
                                strHide.Add("CacheBitmaps")
                                strHide.Add("Colors")
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("Domain")
                                strHide.Add("ExtApp")
                                strHide.Add("ICAEncryption")
                                strHide.Add("Password")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound")
                                strHide.Add("RenderingEngine")
                                strHide.Add("Resolution")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                strHide.Add("Username")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                            Case mRemoteNG.Connection.Protocol.Protocols.Rlogin
                                strHide.Add("CacheBitmaps")
                                strHide.Add("Colors")
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("Domain")
                                strHide.Add("ExtApp")
                                strHide.Add("ICAEncryption")
                                strHide.Add("Password")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound")
                                strHide.Add("RenderingEngine")
                                strHide.Add("Resolution")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                strHide.Add("Username")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                            Case mRemoteNG.Connection.Protocol.Protocols.RAW
                                strHide.Add("CacheBitmaps")
                                strHide.Add("Colors")
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("Domain")
                                strHide.Add("ExtApp")
                                strHide.Add("ICAEncryption")
                                strHide.Add("Password")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound")
                                strHide.Add("RenderingEngine")
                                strHide.Add("Resolution")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                strHide.Add("Username")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                            Case mRemoteNG.Connection.Protocol.Protocols.HTTP
                                strHide.Add("CacheBitmaps")
                                strHide.Add("Colors")
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("Domain")
                                strHide.Add("ExtApp")
                                strHide.Add("ICAEncryption")
                                strHide.Add("PuttySession")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound")
                                strHide.Add("Resolution")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                            Case mRemoteNG.Connection.Protocol.Protocols.HTTPS
                                strHide.Add("CacheBitmaps")
                                strHide.Add("Colors")
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("Domain")
                                strHide.Add("ExtApp")
                                strHide.Add("ICAEncryption")
                                strHide.Add("PuttySession")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound;Resolution")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                            Case mRemoteNG.Connection.Protocol.Protocols.ICA
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("ExtApp")
                                strHide.Add("Port")
                                strHide.Add("PuttySession")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound")
                                strHide.Add("RenderingEngine")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                            Case mRemoteNG.Connection.Protocol.Protocols.IntApp
                                strHide.Add("CacheBitmaps")
                                strHide.Add("Colors")
                                strHide.Add("DisplayThemes")
                                strHide.Add("DisplayWallpaper")
                                strHide.Add("EnableFontSmoothing")
                                strHide.Add("EnableDesktopComposition")
                                strHide.Add("Domain")
                                strHide.Add("ICAEncryption")
                                strHide.Add("PuttySession")
                                strHide.Add("RDGatewayDomain")
                                strHide.Add("RDGatewayHostname")
                                strHide.Add("RDGatewayPassword")
                                strHide.Add("RDGatewayUsageMethod")
                                strHide.Add("RDGatewayUseConnectionCredentials")
                                strHide.Add("RDGatewayUsername")
                                strHide.Add("RDPAuthenticationLevel")
                                strHide.Add("LoadBalanceInfo")
                                strHide.Add("RedirectDiskDrives")
                                strHide.Add("RedirectKeys")
                                strHide.Add("RedirectPorts")
                                strHide.Add("RedirectPrinters")
                                strHide.Add("RedirectSmartCards")
                                strHide.Add("RedirectSound")
                                strHide.Add("RenderingEngine")
                                strHide.Add("Resolution")
                                strHide.Add("AutomaticResize")
                                strHide.Add("UseConsoleSession")
                                strHide.Add("UseCredSsp")
                                strHide.Add("VNCAuthMode")
                                strHide.Add("VNCColors")
                                strHide.Add("VNCCompression")
                                strHide.Add("VNCEncoding")
                                strHide.Add("VNCProxyIP")
                                strHide.Add("VNCProxyPassword")
                                strHide.Add("VNCProxyPort")
                                strHide.Add("VNCProxyType")
                                strHide.Add("VNCProxyUsername")
                                strHide.Add("VNCSmartSizeMode")
                                strHide.Add("VNCViewOnly")
                        End Select

                        If conI.IsDefault = False Then
                            With conI.Inherit
                                If .CacheBitmaps Then
                                    strHide.Add("CacheBitmaps")
                                End If

                                If .Colors Then
                                    strHide.Add("Colors")
                                End If

                                If .Description Then
                                    strHide.Add("Description")
                                End If

                                If .DisplayThemes Then
                                    strHide.Add("DisplayThemes")
                                End If

                                If .DisplayWallpaper Then
                                    strHide.Add("DisplayWallpaper")
                                End If

                                If .EnableFontSmoothing Then
                                    strHide.Add("EnableFontSmoothing")
                                End If

                                If .EnableDesktopComposition Then
                                    strHide.Add("EnableDesktopComposition")
                                End If

                                If .Domain Then
                                    strHide.Add("Domain")
                                End If

                                If .Icon Then
                                    strHide.Add("Icon")
                                End If

                                If .Password Then
                                    strHide.Add("Password")
                                End If

                                If .Port Then
                                    strHide.Add("Port")
                                End If

                                If .Protocol Then
                                    strHide.Add("Protocol")
                                End If

                                If .PuttySession Then
                                    strHide.Add("PuttySession")
                                End If

                                If .RedirectDiskDrives Then
                                    strHide.Add("RedirectDiskDrives")
                                End If

                                If .RedirectKeys Then
                                    strHide.Add("RedirectKeys")
                                End If

                                If .RedirectPorts Then
                                    strHide.Add("RedirectPorts")
                                End If

                                If .RedirectPrinters Then
                                    strHide.Add("RedirectPrinters")
                                End If

                                If .RedirectSmartCards Then
                                    strHide.Add("RedirectSmartCards")
                                End If

                                If .RedirectSound Then
                                    strHide.Add("RedirectSound")
                                End If

                                If .Resolution Then
                                    strHide.Add("Resolution")
                                End If

                                If .AutomaticResize Then strHide.Add("AutomaticResize")

                                If .UseConsoleSession Then
                                    strHide.Add("UseConsoleSession")
                                End If

                                If .UseCredSsp Then
                                    strHide.Add("UseCredSsp")
                                End If

                                If .RenderingEngine Then
                                    strHide.Add("RenderingEngine")
                                End If

                                If .ICAEncryption Then
                                    strHide.Add("ICAEncryption")
                                End If

                                If .RDPAuthenticationLevel Then
                                    strHide.Add("RDPAuthenticationLevel")
                                End If

                                If .LoadBalanceInfo Then strHide.Add("LoadBalanceInfo")

                                If .Username Then
                                    strHide.Add("Username")
                                End If

                                If .Panel Then
                                    strHide.Add("Panel")
                                End If

                                If conI.IsContainer Then
                                    strHide.Add("Hostname")
                                End If

                                If .PreExtApp Then
                                    strHide.Add("PreExtApp")
                                End If

                                If .PostExtApp Then
                                    strHide.Add("PostExtApp")
                                End If

                                If .MacAddress Then
                                    strHide.Add("MacAddress")
                                End If

                                If .UserField Then
                                    strHide.Add("UserField")
                                End If

                                If .VNCAuthMode Then
                                    strHide.Add("VNCAuthMode")
                                End If

                                If .VNCColors Then
                                    strHide.Add("VNCColors")
                                End If

                                If .VNCCompression Then
                                    strHide.Add("VNCCompression")
                                End If

                                If .VNCEncoding Then
                                    strHide.Add("VNCEncoding")
                                End If

                                If .VNCProxyIP Then
                                    strHide.Add("VNCProxyIP")
                                End If

                                If .VNCProxyPassword Then
                                    strHide.Add("VNCProxyPassword")
                                End If

                                If .VNCProxyPort Then
                                    strHide.Add("VNCProxyPort")
                                End If

                                If .VNCProxyType Then
                                    strHide.Add("VNCProxyType")
                                End If

                                If .VNCProxyUsername Then
                                    strHide.Add("VNCProxyUsername")
                                End If

                                If .VNCViewOnly Then
                                    strHide.Add("VNCViewOnly")
                                End If

                                If .VNCSmartSizeMode Then
                                    strHide.Add("VNCSmartSizeMode")
                                End If

                                If .ExtApp Then
                                    strHide.Add("ExtApp")
                                End If

                                If .RDGatewayUsageMethod Then
                                    strHide.Add("RDGatewayUsageMethod")
                                End If

                                If .RDGatewayHostname Then
                                    strHide.Add("RDGatewayHostname")
                                End If

                                If .RDGatewayUsername Then
                                    strHide.Add("RDGatewayUsername")
                                End If

                                If .RDGatewayPassword Then
                                    strHide.Add("RDGatewayPassword")
                                End If

                                If .RDGatewayDomain Then
                                    strHide.Add("RDGatewayDomain")
                                End If

                                If .RDGatewayUseConnectionCredentials Then
                                    strHide.Add("RDGatewayUseConnectionCredentials")
                                End If

                                If .RDGatewayHostname Then
                                    strHide.Add("RDGatewayHostname")
                                End If
                            End With
                        Else
                            strHide.Add("Hostname")
                            strHide.Add("Name")
                        End If
                    ElseIf TypeOf pGrid.SelectedObject Is Root.Info Then
                        Dim rootInfo As Root.Info = CType(pGrid.SelectedObject, Root.Info)
                        If rootInfo.Type = Root.Info.RootType.PuttySessions Then
                            strHide.Add("Password")
                        End If
                    End If

                    Me.pGrid.HiddenProperties = strHide.ToArray

                    Me.pGrid.Refresh()
                Catch ex As Exception
                    MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, My.Language.strConfigPropertyGridHideItemsFailed & vbNewLine & ex.Message, True)
                End Try
            End Sub

            Private Sub btnShowProperties_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowProperties.Click
                If TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Connection.Info.Inheritance Then
                    If TryCast(Me.pGrid.SelectedObject, mRemoteNG.Connection.Info.Inheritance).IsDefault Then
                        Me.PropertiesVisible = True
                        Me.InheritanceVisible = False
                        Me.DefaultPropertiesVisible = False
                        Me.DefaultInheritanceVisible = False
                        Me.SetPropertyGridObject(TryCast(App.Runtime.Windows.treeForm.tvConnections.SelectedNode.Tag, mRemoteNG.Root.Info))
                    Else
                        Me.PropertiesVisible = True
                        Me.InheritanceVisible = False
                        Me.DefaultPropertiesVisible = False
                        Me.DefaultInheritanceVisible = False
                        Me.SetPropertyGridObject(TryCast(Me.pGrid.SelectedObject, mRemoteNG.Connection.Info.Inheritance).Parent)
                    End If
                ElseIf TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Connection.Info Then
                    If TryCast(Me.pGrid.SelectedObject, mRemoteNG.Connection.Info).IsDefault Then
                        Me.PropertiesVisible = True
                        Me.InheritanceVisible = False
                        Me.DefaultPropertiesVisible = False
                        Me.DefaultInheritanceVisible = False
                        Me.SetPropertyGridObject(TryCast(App.Runtime.Windows.treeForm.tvConnections.SelectedNode.Tag, mRemoteNG.Root.Info))
                    End If
                End If
            End Sub

            Private Sub btnShowDefaultProperties_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowDefaultProperties.Click
                If TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Root.Info Or TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Connection.Info.Inheritance Then
                    Me.PropertiesVisible = False
                    Me.InheritanceVisible = False
                    Me.DefaultPropertiesVisible = True
                    Me.DefaultInheritanceVisible = False
                    Me.SetPropertyGridObject(App.Runtime.DefaultConnectionFromSettings())
                End If
            End Sub

            Private Sub btnShowInheritance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowInheritance.Click
                If TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Connection.Info Then
                    Me.PropertiesVisible = False
                    Me.InheritanceVisible = True
                    Me.DefaultPropertiesVisible = False
                    Me.DefaultInheritanceVisible = False
                    Me.SetPropertyGridObject(TryCast(Me.pGrid.SelectedObject, mRemoteNG.Connection.Info).Inherit)
                End If
            End Sub

            Private Sub btnShowDefaultInheritance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowDefaultInheritance.Click
                If TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Root.Info Or TypeOf Me.pGrid.SelectedObject Is mRemoteNG.Connection.Info Then
                    Me.PropertiesVisible = False
                    Me.InheritanceVisible = False
                    Me.DefaultPropertiesVisible = False
                    Me.DefaultInheritanceVisible = True
                    Me.SetPropertyGridObject(App.Runtime.DefaultInheritanceFromSettings())
                End If
            End Sub

            Private Sub btnHostStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHostStatus.Click
                SetHostStatus(Me.pGrid.SelectedObject)
            End Sub

            Private Sub btnIcon_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnIcon.MouseUp
                Try
                    If TypeOf pGrid.SelectedObject Is mRemoteNG.Connection.Info And _
                       Not TypeOf pGrid.SelectedObject Is mRemoteNG.Connection.PuttySession.Info Then
                        Me.cMenIcons.Items.Clear()

                        For Each iStr As String In mRemoteNG.Connection.Icon.Icons
                            Dim tI As New ToolStripMenuItem
                            tI.Text = iStr
                            tI.Image = mRemoteNG.Connection.Icon.FromString(iStr).ToBitmap
                            AddHandler tI.Click, AddressOf IconMenu_Click

                            Me.cMenIcons.Items.Add(tI)
                        Next

                        Dim mPos As New Point(PointToScreen(New Point(e.Location.X + Me.pGrid.Width - 100, e.Location.Y)))
                        Me.cMenIcons.Show(mPos)
                    End If
                Catch ex As Exception
                    MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, My.Language.strConfigPropertyGridButtonIconClickFailed & vbNewLine & ex.Message, True)
                End Try
            End Sub

            Private Sub IconMenu_Click(ByVal sender As Object, ByVal e As EventArgs)
                Try
                    Dim connectionInfo As mRemoteNG.Connection.Info = TryCast(pGrid.SelectedObject, mRemoteNG.Connection.Info)
                    If connectionInfo Is Nothing Then Return

                    Dim selectedMenuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
                    If selectedMenuItem Is Nothing Then Return

                    Dim iconName As String = selectedMenuItem.Text
                    If String.IsNullOrEmpty(iconName) Then Return

                    Dim connectionIcon As Icon = mRemoteNG.Connection.Icon.FromString(iconName)
                    If connectionIcon Is Nothing Then Return

                    btnIcon.Image = connectionIcon.ToBitmap()

                    connectionInfo.Icon = iconName
                    pGrid.Refresh()

                    'SaveConnectionsBG()
                Catch ex As Exception
                    MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, Language.strConfigPropertyGridMenuClickFailed & vbNewLine & ex.Message, True)
                End Try
            End Sub
#End Region

#Region "Host Status (Ping)"
            Private HostName As String
            Private pThread As Threading.Thread

            Private Sub CheckHostAlive()
                Dim pingSender As New Ping
                Dim pReply As PingReply

                Try
                    pReply = pingSender.Send(HostName)

                    If pReply.Status = IPStatus.Success Then
                        If Me.btnHostStatus.Tag = "checking" Then
                            ShowStatusImage(My.Resources.HostStatus_On)
                        End If
                    Else
                        If Me.btnHostStatus.Tag = "checking" Then
                            ShowStatusImage(My.Resources.HostStatus_Off)
                        End If
                    End If
                Catch ex As Exception
                    If Me.btnHostStatus.Tag = "checking" Then
                        ShowStatusImage(My.Resources.HostStatus_Off)
                    End If
                End Try
            End Sub

            Delegate Sub ShowStatusImageCB(ByVal [Image] As Image)
            Private Sub ShowStatusImage(ByVal [Image] As Image)
                If Me.pGrid.InvokeRequired Then
                    Dim d As New ShowStatusImageCB(AddressOf ShowStatusImage)
                    Me.pGrid.Invoke(d, New Object() {[Image]})
                Else
                    Me.btnHostStatus.Image = Image
                    Me.btnHostStatus.Tag = "checkfinished"
                End If
            End Sub

            Public Sub SetHostStatus(ByVal ConnectionInfo As Object)
                Try
                    Me.btnHostStatus.Image = My.Resources.HostStatus_Check

                    ' To check status, ConnectionInfo must be an mRemoteNG.Connection.Info that is not a container
                    If TypeOf ConnectionInfo Is mRemoteNG.Connection.Info Then
                        If TryCast(ConnectionInfo, mRemoteNG.Connection.Info).IsContainer Then Return
                    Else
                        Return
                    End If

                    Me.btnHostStatus.Tag = "checking"
                    HostName = TryCast(ConnectionInfo, mRemoteNG.Connection.Info).Hostname
                    pThread = New Threading.Thread(AddressOf CheckHostAlive)
                    pThread.SetApartmentState(Threading.ApartmentState.STA)
                    pThread.IsBackground = True
                    pThread.Start()
                Catch ex As Exception
                    MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, My.Language.strConfigPropertyGridSetHostStatusFailed & vbNewLine & ex.Message, True)
                End Try
            End Sub
#End Region

            Private Sub propertyGridContextMenu_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles propertyGridContextMenu.Opening
                Try
                    propertyGridContextMenuShowHelpText.Checked = Settings.ShowConfigHelpText
                    Dim gridItem As GridItem = pGrid.SelectedGridItem
                    propertyGridContextMenuReset.Enabled = (pGrid.SelectedObject IsNot Nothing AndAlso _
                                                            gridItem IsNot Nothing AndAlso _
                                                            gridItem.PropertyDescriptor IsNot Nothing AndAlso _
                                                            gridItem.PropertyDescriptor.CanResetValue(pGrid.SelectedObject))
                Catch ex As Exception
                    MessageCollector.AddExceptionMessage("UI.Window.Config.propertyGridContextMenu_Opening() failed.", ex, MessageClass.ErrorMsg, True)
                End Try
            End Sub

            Private Sub propertyGridContextMenuReset_Click(sender As System.Object, e As EventArgs) Handles propertyGridContextMenuReset.Click
                Try
                    Dim gridItem As GridItem = pGrid.SelectedGridItem
                    If pGrid.SelectedObject IsNot Nothing AndAlso _
                            gridItem IsNot Nothing AndAlso _
                            gridItem.PropertyDescriptor IsNot Nothing AndAlso _
                            gridItem.PropertyDescriptor.CanResetValue(pGrid.SelectedObject) Then
                        pGrid.ResetSelectedProperty()
                    End If
                Catch ex As Exception
                    MessageCollector.AddExceptionMessage("UI.Window.Config.propertyGridContextMenuReset_Click() failed.", ex, MessageClass.ErrorMsg, True)
                End Try
            End Sub

            Private Sub propertyGridContextMenuShowHelpText_Click(sender As Object, e As EventArgs) Handles propertyGridContextMenuShowHelpText.Click
                propertyGridContextMenuShowHelpText.Checked = Not propertyGridContextMenuShowHelpText.Checked
            End Sub

            Private Sub propertyGridContextMenuShowHelpText_CheckedChanged(sender As Object, e As EventArgs) Handles propertyGridContextMenuShowHelpText.CheckedChanged
                Settings.ShowConfigHelpText = propertyGridContextMenuShowHelpText.Checked
                pGrid.HelpVisible = propertyGridContextMenuShowHelpText.Checked
            End Sub
        End Class
    End Namespace
End Namespace